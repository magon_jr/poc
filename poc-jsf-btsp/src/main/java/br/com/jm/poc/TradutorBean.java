package br.com.jm.poc;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.rmtheis.yandtran.YandexTranslatorAPI;
import com.rmtheis.yandtran.language.Language;
import com.rmtheis.yandtran.translate.Translate;

@ManagedBean(name = "tradutorBean")
@ViewScoped
public class TradutorBean implements Serializable {
	private String traducao;
	private String texto;

	public String getTraducao() {
		return traducao;
	}

	public void setTraducao(String traducao) {
		this.traducao = traducao;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public void traduzir(String de, String para) {
		Translate.setKey("[Sua appKey aqui]]");

		try {
			traducao = Translate.execute(texto,
					Language.fromString(de), Language.fromString(para));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
